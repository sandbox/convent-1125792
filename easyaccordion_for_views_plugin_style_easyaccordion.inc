<?php

/**
 * Style plugin to render each item of a EasyAccordion and to handle the options form.
 */
class easyaccordion_for_views_plugin_style_easyaccordion extends views_plugin_style_list {
  // If the view is still using the old variables replace with the new ones.
  function init(&$view, &$display, $options = NULL) {
    // These are required for the view to continue to work.
    $this->view = &$view;
    $this->display = &$display;

    // Overlay incoming options on top of defaults.
    $this->unpack_options($this->options, isset($options) ? $options : $display->handler->get_option('style_options'));

    if ($this->uses_row_plugin() && $display->handler->get_option('row_plugin')) {
      $this->row_plugin = $display->handler->get_plugin('row');
    }
  }

  // Set default options.
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  // Render the given style.
  function options_form(&$form, &$form_state) {
    $skin_options = array();
    foreach (easyaccordion_for_views_get_available_skins() as $skin_id => $skin) {
      $name = $skin['meta']['Skin Name'];
      if (!$name) {
        $name = drupal_ucfirst($skin_id);
      }
      $skin_options[$skin_id] = $name;
    }
    $form['easyaccordion_skin'] = array(
      '#title' => t('Skin'),
      '#type' => 'radios',
      '#description' => t('Choose a skin.'),
      '#default_value' => ($this->options['easyaccordion_skin'] ? $this->options['easyaccordion_skin'] : 'easyaccordion'),
      '#options' => $skin_options,
    );
	if (isset($this->view->display[$this->view->current_display]->display_options['fields'])) {
      $fields = $this->view->display[$this->view->current_display]->display_options['fields'];
	}
    else {
      $fields = $this->view->display['default']->display_options['fields'];
    }
    $form['easyaccordion_title_field'] = array(
      '#title' => t('Which field do you want to use as your title?'),
      '#type' => 'radios',
      '#description' => t("This is the content that will be used as each EasyAccordion panel's title."),
      '#default_value' => ($this->options['easyaccordion_title_field'] ? $this->options['easyaccordion_title_field'] : 'none'),
      '#options' => array('none' => '(none)') + drupal_map_assoc(array_keys($fields)),
    );
	$form['easyaccordion_slidenum'] = array(
      '#title' => t('Show slide numbers?'),
      '#type' => 'checkbox',
      '#description' => t("Show or remove slidenumbers."),
      '#default_value' => ($this->options['easyaccordion_slidenum'] ? $this->options['easyaccordion_slidenum'] : TRUE),
    );
	$form['easyaccordion_autostart'] = array(
      '#title' => t('Enable Autoplay?'),
      '#type' => 'checkbox',
      '#description' => t("With Autoplay enabled, the plugin will move to the next slide every Slideinterval milliseconds."),
      '#default_value' => ($this->options['easyaccordion_autostart'] ? $this->options['easyaccordion_autostart'] : FALSE),
    );
	$form['easyaccordion_slideinterval'] = array(
	  '#type' => 'textfield',
	  '#title' => t('Slideinterval'),
	  '#description' => t("Move to next slide every n milliseconds"),
	  '#default_value' => ($this->options['easyaccordion_slideinterval'] ? $this->options['easyaccordion_slideinterval'] : '5000'),
    );
  }
  
}
