<?php

/**
 * Implements hook_views_plugins().
 */
function easyaccordion_for_views_views_plugins() {
  return array(
    'style' => array(
      'easyaccordion_for_views' => array(
        'title' => t('EasyAccordion'),
        'help' => t('Display the results as a EasyAccordion.'),
        'handler' => 'easyaccordion_for_views_plugin_style_easyaccordion',
        'theme' => 'easyaccordion_for_views',
        'uses options' => TRUE,
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'type' => 'normal',
        'parent' => 'list',
      ),
    ),
  );
}
